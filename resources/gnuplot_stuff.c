#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char** argv) {
  int ndata=atoi(argv[1]);

  while (1) {
    printf("plot '-' w l\n");
    for (int i=0; i<ndata; ++i) {
      printf("%d %f\n",i, drand48());
    }
    printf("e\n\n");
  }
}
