#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/joystick.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char** argv) {
  int fd = open(argv[1], O_RDONLY);
  struct js_event e;
  while(1) {
    while (read (fd, &e, sizeof(e)) > 0 ){
      printf ("EVENT time: %u, axis: %d, type: %d, value: %d\n", e.time, e.number,
	      e.type,
	      e.value);
    }
  }
}
